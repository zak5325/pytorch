#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 23 15:50:16 2019

@author: lirt
"""
import torch
from torch.utils.data import DataLoader
from torchvision.datasets import MNIST
from torchvision import transforms
from torch import optim
import torch.nn as nn
from tqdm import tqdm

def loadMNIST(batch_size):  #MNIST圖片的大小是28*28
    trans_img=transforms.Compose([transforms.ToTensor()])
    trainset=MNIST('./data_lenet',train=True,transform=trans_img,download=True)
    testset=MNIST('./data_lenet',train=False,transform=trans_img,download=True)
    # device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    trainloader=DataLoader(trainset,batch_size=batch_size,shuffle=True,num_workers=10)
    testloader = DataLoader(testset, batch_size=batch_size, shuffle=False, num_workers=10)
    return trainset,testset,trainloader,testloader

class LeNet(nn.Module):
    def __init__(self):
        super(LeNet, self).__init__() #調用基類的__init__()函数
        #torch.nn.Conv2d(in_channels, out_channels, kernel_size, stride=1, padding=0, dilation=1, groups=1, bias=True)
        #torch.nn.MaxPool2d(kernel_size, stride=None, padding=0, dilation=1, return_indices=False, ceil_mode=False)
        #stride – the stride of the window. Default value is kernel_size
        self.conv=nn.Sequential( #順序網路結構
            nn.Conv2d(1,6,5,stride=1,padding=2), #卷積層 輸入1通道，輸出6通道，kernel_size=5*5
            nn.ReLU(),         #啟動函數
            nn.MaxPool2d(2,2),     #最大池化，kernel_size=2*2，stride=2*2
            #輸出大小为14*14
            nn.Conv2d(6,16,5,stride=1), #卷積層 輸入6通道，輸出16通道，kernel_size=5*5
            nn.ReLU(),
            nn.MaxPool2d(2,2),
            # 輸出大小为5*5
        )
        self.fc=nn.Sequential( #全聯接層
            nn.Linear(5*5*120,120),
            nn.ReLU(),
            nn.Linear(120, 84),
            nn.ReLU(),
            nn.Linear(84,10),
            nn.Sigmoid(),
        )
    def forward(self, x):  #前向傳播
        out=self.conv(x)
        out=out.view(out.size(0),-1) #平坦化為一維向量
        out=self.fc(out)
        return out

learning_rate=1e-3
batch_size=100
epoches=50
lenet=LeNet()#network
trainset,testset,trainloader,testloader=loadMNIST(batch_size)#data
criterian=nn.CrossEntropyLoss(size_average=False) #loss
optimizer=optim.SGD(lenet.parameters(),lr=learning_rate) #optimizer
lenet.to("cpu")

#training
for i in tqdm(range(epoches)):
    running_loss=0.
    running_acc=0.
    for (img,label) in trainloader:
        optimizer.zero_grad()  #求梯度之前对梯度清零以防梯度累加
        output=lenet(img) #前項計算輸出值
        loss=criterian(output,label) #計算loss值
        loss.backward()    #loss反向傳播存到相對應的權重
        optimizer.step()   #使用計算好的梯度修正量對權重進行更新
 
        running_loss+=loss.item()
        #print(output)
        _,predict=torch.max(output,1)
        correct_num=(predict==label).sum()
        running_acc+=correct_num.item()
    running_loss/=len(trainset)
    running_acc/=len(trainset)
    print("[%d/%d] Loss: %.5f, Acc: %.2f"%(i+1,epoches,running_loss,100*running_acc))

total_cnt=0
correct_cnt=0
for (img,label) in testloader:
    output=lenet(img)
    _, predict = torch.max(output, 1)
    for i in range(len(predict)):
        total_cnt+=1
        if predict[i]==label[i]:
            correct_cnt+=1
print(correct_cnt,total_cnt,correct_cnt/total_cnt)

torch.save(lenet.state_dict(), "lenet_mnist.pt")