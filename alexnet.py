#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 25 09:46:40 2019

@author: lirt
"""
import torch
from torch.utils.data import DataLoader
from torchvision.datasets import CIFAR10
from torchvision import transforms
from torch import optim
import torch.nn as nn
from tqdm import tqdm

def loadCIFAR(batch_size):  #MNIST图片的大小是32*32
    trans_img=transforms.Compose([transforms.ToTensor()])
    trainset=CIFAR10('./data_alexnet',train=True,transform=trans_img,download=True)
    testset=CIFAR10('./data_alexnet',train=False,transform=trans_img,download=True)
    # device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    trainloader=DataLoader(trainset,batch_size=batch_size,shuffle=True,num_workers=10)
    testloader = DataLoader(testset, batch_size=batch_size, shuffle=False, num_workers=10)
    return trainset,testset,trainloader,testloader

class AlexNet(nn.Module):
    def __init__(self):
        super(AlexNet, self).__init__() #调用基类的__init__()函数
        #torch.nn.Conv2d(in_channels, out_channels, kernel_size, stride=1, padding=0, dilation=1, groups=1, bias=True)
        #torch.nn.MaxPool2d(kernel_size, stride=None, padding=0, dilation=1, return_indices=False, ceil_mode=False)
        #stride – the stride of the window. Default value is kernel_size
        self.conv=nn.Sequential( #順序網路結構
            nn.Conv2d(3,48,3,stride=1), #卷積層 輸入3通道，輸出48通道，kernel_size=3*3 
            nn.ReLU(),         #啟動函數
            nn.MaxPool2d(2,2),     #最大池化，kernel_size=2*2，stride=2*2
            #輸出大小為 15*15
            nn.Conv2d(48,96,3,stride=1,padding=1), #卷積層 輸入48通道，輸出256通道，kernel _size=3*3
            nn.ReLU(),
            nn.MaxPool2d(2,2),
            # 輸出大小為 7*7
            nn.Conv2d(96,192,3,stride=1,padding=1), #卷積層 輸入48通道，輸出256通道，kernel_size=3*3
            nn.ReLU(),
            nn.Conv2d(192,192,3,stride=1,padding=1), #卷積層 輸入48通道，輸出256通道，kernel_size=3*3
            nn.ReLU(),
            nn.Conv2d(192,256,3,stride=1,padding=1), #卷積層 輸入48通道，輸出256通道，kernel_size=3*3
            nn.ReLU(),
            nn.MaxPool2d(2,2),
            #輸出大小為 3*3
        )
        self.classifier=nn.Sequential( #全連接層
            nn.Linear(3*3*256,512),
            nn.ReLU(),
            nn.Linear(512, 256),
            nn.ReLU(),
            nn.Linear(256,10),
            nn.Sigmoid(),
        )
    def forward(self, x):  #前向传播
        out=self.conv(x)
        out=out.view(out.size(0),-1) #平坦化
        out=self.classifier(out)
        return out

learning_rate=1e-3
batch_size=100
epoches=50
alexnet=AlexNet()#network
trainset,testset,trainloader,testloader=loadCIFAR(batch_size)#data
criterian=nn.CrossEntropyLoss(size_average=False) #loss
optimizer=optim.SGD(alexnet.parameters(),lr=learning_rate) #optimizer

#training
for i in tqdm(range(epoches)):
    running_loss=0.
    running_acc=0.
    for (img,label) in trainloader:
        optimizer.zero_grad()  #求梯度之前對梯度清零以防梯度累加
        output=alexnet(img) #前項計算輸出值
        loss=criterian(output,label) #計算loss值
        loss.backward()    #loss反向傳播存到相對應的權重
        optimizer.step()   #使用計算好的梯度修正量對權重進行更新
        running_loss+=loss.item()
        #print(output)
        _,predict=torch.max(output,1)
        correct_num=(predict==label).sum()
        running_acc+=correct_num.item()
    running_loss/=len(trainset)
    running_acc/=len(trainset)
    print("[%d/%d] Loss: %.5f, Acc: %.2f"%(i+1,epoches,running_loss,100*running_acc))

total_cnt=0
correct_cnt=0
for (img,label) in testloader:
    output=alexnet(img)
    _, predict = torch.max(output, 1)
    for i in range(len(predict)):
        total_cnt+=1
        if predict[i]==label[i]:
            correct_cnt+=1
print(correct_cnt,total_cnt,correct_cnt/total_cnt)

torch.save(alexnet.state_dict(), "./data_alexnet/alexnet_cifar10.pt")
